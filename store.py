from sanic import Sanic
from sanic.response import json, text
from db import connect_to_db, close_db

app = Sanic(__name__)

@app.listener('before_server_start')
async def setup_db(app, loop):
    await connect_to_db(app, loop)

@app.listener('after_server_stop')
async def close_db_listener(app, loop):
    await close_db(app, loop)

# GET /store/inventory
@app.route('/store/inventory', methods=['GET'])
async def get_inventory(request):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('SELECT * FROM Pet WHERE status = %s', (request.args.get('status'),))
                result = await cur.fetchall()
        return json(result)
    except Exception as e:
        return json({'error': str(e)}, status=500)

#GET /store/order/{orderId}
@app.route('/store/order/<orderId>', methods=['GET'])
async def get_order(request, orderId):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                
                await cur.execute('SELECT * FROM Orderr WHERE id = %s', (orderId,))
                result = await cur.fetchone()
        if result:
            return json(result)
        else:
            return text('Order not found', status=404)
    except Exception as e:
        return json({'error': str(e)}, status=500)

#DELETE /store/order/{orderId}
@app.route('/store/order/<orderId>', methods=['DELETE'])
async def delete_order(request, orderId):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                
                await cur.execute('DELETE FROM Orderr WHERE id = %s', (orderId,))
                await conn.commit()
        return text('Order deleted successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)

#POST /store/order
@app.route('/store/order', methods=['POST'])
async def place_order(request):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                data = await request.json()
                await cur.execute('INSERT INTO Orderr (id, petId, quantity, shipDate, status) VALUES (%s, %s, %s, %s, %s)',
                                  (data['id'], data['petId'], data['quantity'], data['shipDate'], data['status']))
                await conn.commit()
        return text('Order placed successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
    