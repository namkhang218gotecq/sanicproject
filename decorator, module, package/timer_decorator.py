
# timer_decorator.py - module định nghĩa decorator để đo thời gian thực thi của hàm
import time

def timer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f'Thời gian thực thi file {func.__name__}.py là: {end_time - start_time:.4f} giây')
        return result
    return wrapper
