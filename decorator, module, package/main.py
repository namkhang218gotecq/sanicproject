
# main.py - module sử dụng module rectangle và decorator timer
from rectangle import get_rectangle_length
from timer_decorator import timer

@timer
def main():
    length = 5
    width = 10
    rectangle_length = get_rectangle_length(length, width)
    print(f'Chiều dài của hình chữ nhật: {rectangle_length}')

if __name__ == '__main__':
    main()
