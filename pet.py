from sanic import Sanic
from sanic.response import json, text
    
from db import connect_to_db, close_db

app = Sanic(__name__)


@app.listener('before_server_start')
async def setup_db(app, loop):
    await connect_to_db(app, loop)

@app.listener('after_server_stop')
async def close_db_listener(app, loop):
    await close_db(app, loop)

# Get All /pets
@app.route('/pets', methods=['GET'])
async def get_pets(request):
        # Lấy session đến CSDL
    async with app.ctx.pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('SELECT * FROM Pet')
            result = await cur.fetchall()
    return json(result)


# API GET /pet/{petId}
@app.route('/pet/<petId:int>', methods=['GET'])
async def get_pet(request, petId):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('SELECT * FROM Pet WHERE id = %s', (petId,))
                result = await cur.fetchall()

        if result:
            return json(result[0])
        else:
            return text('Pet not found', status=404)
    except Exception as e:
        return json({'error': str(e)}, status=500)
    
# API DELETE /pet/{petId}
@app.route('/pet/<petId:int>', methods=['DELETE'])
async def delete_pet(request, petId):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                
                await cur.execute('UPDATE Pet SET categoryId = NULL, tagId = NULL WHERE id = %s', (petId,))
                await conn.commit()

                await cur.execute('DELETE FROM Pet WHERE id = %s', (petId,))
                await conn.commit()

        return text('Pet deleted successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)
# 
# API POST /pet/{petId}/UploadImage 
@app.route('/pet/<petId:int>/UploadImage', methods=['POST'])
async def upload_image(request, petId):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('SELECT * FROM Pet WHERE id = %s', (petId,))
                result = await cur.fetchall()
                if not result:
                    return text('Pet not found', status=404)

                return text('Image uploaded successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)

# API POST /pet 
@app.route('/pet', methods=['POST'])
async def add_pet(request):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                data = await request.json()
                # chuyển "photoUrls" thành chuỗi JSON 
                photoUrls = json.dumps(data['photoUrls'])
                
                await cur.execute('INSERT INTO Pet (id, name, photoUrls, status) VALUES (%s, %s, %s, %s)', (data['id'],data['name'], photoUrls ,data['status']))
                await conn.commit()

        return text('Pet added successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)



# API PUT /pet
@app.route('/pet', methods=['PUT'])
async def update_pet(request):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                data = await request.json()
                await cur.execute('UPDATE Pet SET name = %s, status = %s WHERE id = %s', (data['name'], data['status'], data['id']))
                await conn.commit()

        return text('Pet updated successfully')
    except Exception as e:
        return json({'error': str(e)}, status=500)


# API GET /pet/findByStatus
@app.route('/pet/findByStatus', methods=['GET'])
async def find_pet_by_status(request):
    try:
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                status = request.args.get('status')
                await cur.execute('SELECT * FROM Pet WHERE status = %s', (status,))
                result = await cur.fetchall()
        return json(result)
    except Exception as e:
        return json({'error': str(e)}, status=500)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
