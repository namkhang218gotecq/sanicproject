from sanic import Sanic
from sanic.response import json, text
from db import connect_to_db, close_db

app = Sanic(__name__)

@app.listener('before_server_start')
async def setup_db(app, loop):
    await connect_to_db(app, loop)

@app.listener('after_server_stop')
async def close_db_listener(app, loop):
    await close_db(app, loop)

# Get All /users
@app.route('/users', methods=['GET'])
async def get_users(request):
    async with app.ctx.pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('SELECT * FROM User')
            result = await cur.fetchall()
    return json(result)

# GET /user/{username}
@app.route('/user/<username>', methods=['GET'])
async def get_user(request, username):
    async with app.ctx.pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('SELECT * FROM User WHERE username = %s', (username,))
            result = await cur.fetchall()
    if result:
        return json(result[0])
    else:
        return text('User not found', status=404)

# PUT /user/{username}
@app.route('/user/<username>', methods=['PUT'])
async def update_user(request, username):
    try:
        data = request.json
        if data is None:
            return text('Invalid input data', status=400)
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('UPDATE User SET firstName = %s, email = %s WHERE username = %s',
                                  (data['firstName'], data['email'], username))
                await conn.commit()
        return text('User updated successfully')
    except Exception as e:
        return text(str(e), status=500)

# DELETE /user/{username}
@app.route('/user/<username>', methods=['DELETE'])
async def delete_user(request, username):
    async with app.ctx.pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('DELETE FROM User WHERE username = %s', (username,))
            await conn.commit()
    return text('User deleted successfully')

# POST /user 
@app.route('/user', methods=['POST'])
async def create_user(request):
    try:
        data = request.json
        if data is None:
            return text('Invalid input data', status=400)
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('INSERT INTO User (username, firstName, lastName, email, password, phone, userStatus) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                                  (data['username'], data['firstName'], data['lastName'], data['email'],data['password'],data['phone'],data['userStatus']))
                await conn.commit()
        return json({'message': 'Users created successfully', 'users': "created_users"}, status=201)
    except Exception as e:
        return text(str(e), status=500)

# POST /user/createWithArray 
@app.route('/user/createWithArray', methods=['POST'])
async def create_users_with_array(request):
    try:
        data = request.json
        if data is None or not isinstance(data, list):
            return text('Invalid input data', status=400)
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                for user in data: #Loop lis user, add user about User Table
                    await cur.execute('INSERT INTO User (username, firstName, lastName, email, password, phone, userStatus) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                                      (user['username'], user['firstName'], user['lastName'], user['email'],user['password'],user['phone'],user['userStatus']))
                await conn.commit()
        return text('Users created successfully', status=201)
    except Exception as e:
        return text(str(e), status=500)

#POST /user/createWithList
@app.route('/user/createWithList', methods=['POST'])
async def create_users_with_list(request):
    try:
        data = request.json
        if data is None or not isinstance(data, list):
            return text('Invalid input data', status=400)
        async with app.ctx.pool.acquire() as conn:
            async with conn.cursor() as cur:
                for user in data:
                    await cur.execute('INSERT INTO User (username, firstName, lastName, email, password, phone, userStatus) VALUES (%s, %s, %s, %s, %s, %s, %s)',
                                      (user['username'], user['firstName'], user['lastName'], user['email'],user['password'],user['phone'],user['userStatus']))
                await conn.commit()
        return text('Users created successfully', status=201)
    except Exception as e:
        return text(str(e), status=500)

#GET /user/login
@app.route('/user/login', methods=['GET'])
async def user_login(request):
    try:
        #check username and password có khớp trong db hay không?
        return text('User login successfully', status=200)
    except Exception as e:
        return text(str(e), status=500) 

#GET /user/logout 
@app.route('/user/logout', methods=['GET'])
async def user_logout(request):
    try:
        # Cách xoá phiên đăng nhập (session)
        return text('User logout successfully', status=200)
    except Exception as e:
        return text(str(e), status=500)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
