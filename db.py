import aiomysql

async def connect_to_db(app, loop):
    db_config = {
        'host': 'localhost',
        'port': 3306,
        'user': 'root',
        'password': 'Phan-khang21082001',
        'db': 'pet_store',
        'autocommit': True,
        'minsize': 1,
        'maxsize': 10
    }
    app.ctx.pool = await aiomysql.create_pool(**db_config, loop=loop)

async def close_db(app, loop):
    app.ctx.pool.close()
    await app.ctx.pool.wait_closed()
